export OF_VANILLA_BUILD=1
export OF_DONT_PATCH_ON_FRESH_INSTALLATION=1
export OF_SKIP_ORANGEFOX_PROCESS=1
export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1
export FOX_USE_LZMA_COMPRESSION=0
export OF_FLASHLIGHT_ENABLE=0
export FOX_R11=1
export OF_ALLOW_DISABLE_NAVBAR=1
export OF_USE_TWRP_SAR_DETECT=1
export OF_DISABLE_EXTRA_ABOUT_PAGE=1
export OF_USE_TWRP_SAR_DETECT=1
export OF_DISABLE_KEYMASTER2=1
export OF_LEGACY_SHAR512=1
export FOX_RECOVERY_INSTALL_PARTITION=/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY
export FOX_RECOVERY_SYSTEM_PARTITION=/dev/block/platform/13540000.dwmmc0/by-name/SYSTEM
export FOX_VERSION=R11.0
export FOX_BUILD_TYPE=Beta
export OF_USE_MAGISKBOOT=1
export OF_USE_MAGISKBOOT_FOR_ALL_PATCHES=1


add_lunch_combo omni_j4ltejx-eng
add_lunch_combo omni_j4ltejx-userdebug
